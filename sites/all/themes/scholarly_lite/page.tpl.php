<?php if (theme_get_setting('scrolltop_display')): ?>
<div id="toTop"><i class="fa fa-angle-up"></i></div>
<?php endif; ?>

<!-- #header -->
<header id="header" class="clearfix">
    <!-- #header-inside -->
    <div id="header-inside" class="clearfix">
        <!-- <div class="row"> -->
            <div class="col-md-16">
                <div id="header-inside-left" class="clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2">
                                <?php if ($logo):?>
                                <div id="logo">
                                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"> <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /> </a>
                                </div>
                                <?php endif; ?>
                                
                                <div style="display:inline-block; width:100%;">
                                <?php if ($page['search_bar']):?>
                                    <?php print render($page['search_bar']); ?>
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-7" style="padding-top:3%;">
                                <?php if ($site_name):?>
                                <div id="site-name">
                                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
                                </div>
                                <?php endif; ?>
                                
                                <?php if ($site_slogan):?>
                                <div id="site-slogan">
                                    <?php print $site_slogan; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-3 responsive">
                                <?php if ($page['user_information']) :?>
                                    <?php if (!$GLOBALS["user"]->uid) :?>
                                        <div class="content">
                                            <h3 style="color: #095EA3;">Вход на сайт</h3>
                                            <form action="/frontpage?destination=frontpage" method="post" id="user-login-form" accept-charset="UTF-8">
                                                <div class="form-item form-type-textfield form-item-name">
                                                    <input type="text" id="edit-name" name="name" value="" size="15" maxlength="60" class="form-text required" placeholder="Эл. почта / Имя Фамилия" />
                                                </div>
                                                <div class="form-item form-type-password form-item-pass">
                                                    <input type="password" id="edit-pass" name="pass" size="15" maxlength="128" class="form-text required" placeholder="Пароль" />
                                                </div>
                                                <input type="hidden" name="form_build_id" value="form-mp_m_FxAGojoVlhNTWdMoG-7C0MMJnPumNiPywihWHE" />
                                                <input type="hidden" name="form_id" value="user_login_block" />
                                                <div class="item-list text-uppercase" id="login_form">
                                                	<input type="submit" id="edit-submit--2" name="op" value="Войти" class="form-submit" /><br/>
                                                    <a href="/user/register" id="a_color" title="Создать новую учётную запись пользователя.">Регистрация</a><br/>
                                                    <a href="/user/password" title="Запросить новый пароль по e-mail.">Восстановить пароль</a>
                                                    <!-- <a href="#" class="nav-link" data-toggle="modal" data-target="#exampleModal">РЕГИСТРАЦИЯ</a> -->
                                                </div>
                                            </form>
                                        </div>
                                        <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">ыы</h5>
                                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="container-fluid">
                                                            <form method="post">
                                                                <div class="form-row">
                                                                    <div class="col">
                                                                        <div class="form-group">
                                                                            <label for="examleInputEmail">Имя</label>
                                                                            <input type="text" name="name" class="form-control" id="examleInputEmail" aria-describdby="emailHelp" placeholder="как тебя зовут?" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-check">
                                                                    <label class="form-check-label">
                                                                        <input type="checkbox" class="form-check-input">
                                                                        Запомнить
                                                                    </label>
                                                                </div>
                                                                <button type="submit" name="sent" class="btn btn-primary">Отправить</button>
                                                            </form>
                                                        </div>
					                                </div>
                                                </div>	 	 
                                            </div>
                                        </div> -->
                                    <?php else : ?>
                                        <div>
                                        	<div class="col-md-12 text-center responsive">
                                            	<h5>Вы вошли как</h5>
                                        	</div>
                                            <div class="col-md-4 col-md-push-6 padding-0">
	                                        <!-- Фото пользователя -->
	                                        <?php
							                $user = user_load($user->uid);
							                if ($user->picture) {
							                	print theme_image_style(
							                		array(
							                			'style_name' => 'thumbnail',
							                			'path' => $user->picture->uri,
							                			'attributes' => array(
							                				'class' => 'avatar'
							                			),
							                			'width' => NULL,
							                			'height' => NULL,
							                		)
							                	);
							                } else {
							                    echo '<img src="/sites/default/files/styles/medium/public/pictures/picture-default.png" />';
							                }
	            							?>
                                        	</div>
  											<div class="col-md-8 col-md-pull-6 responsive text-right">
	                                            <a href="/user"><u><?= $GLOBALS['user']->name ?></a></u><br/>
	                                            <small><a href="/user/logout">Выйти</a></small>
  											</div>
                                        </div>
                                    <?php endif; ?> 
                                <?php endif; ?> 
                            </div>
                        </div>
                    </div>
                    <!-- ## Full width header ## -->
                    <script type="text/javascript" src="/sites/all/themes/scholarly_lite/js/slider.js"></script>
                        <style>
                        .jssorl-009-spin img{animation-name:jssorl-009-spin;animation-duration:1.6s;animation-iteration-count:infinite;animation-timing-function:linear}@keyframes jssorl-009-spin{from{transform:rotate(0);}to{transform:rotate(360deg);}}.jssorb032{position:absolute}.jssorb032 .i{position:absolute;cursor:pointer}.jssorb032 .i .b{fill:#fff;fill-opacity:.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:.25}.jssorb032 .i:hover .b{fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35}.jssorb032 .iav .b{fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35}.jssorb032 .i.idn{opacity:.3}.jssora051{display:block;position:absolute;cursor:pointer}.jssora051 .a{fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10}.jssora051:hover{opacity:.8}.jssora051.jssora051dn{opacity:.5}.jssora051.jssora051ds{opacity:.3;pointer-events:none}
                        </style>
                        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:230px;overflow:hidden;visibility:hidden;">
                            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:230px;overflow:hidden;">
                                <div data-p="225.00">
                                    <img data-u="image" src="/sites/all/themes/scholarly_lite/images/header/header1.jpg" />
                                </div>
                                <div data-p="225.00">
                                    <img data-u="image" src="/sites/all/themes/scholarly_lite/images/header/header2.jpg" />
                                </div>
                            </div>
                            <!-- Bullet Navigator -->
                            <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                                <div data-u="prototype" class="i" style="width:16px;height:16px;">
                                    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                        <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                                    </svg>
                                </div>
                            </div>
                            <!-- Arrow Navigator -->
                            <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                            </svg>
                            </div>
                            <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                            </svg>
                            </div>
                        </div>
                        <script type="text/javascript">jssor_1_slider_init();</script>
                </div>
            </div>
            <div class="container">
                <div class="col-md-12">
                    <div class="clearfix">
                        <!-- #main-navigation -->
                        <div id="main-navigation" class="text-center">
                            <nav>
                                <?php print render($page['navigation']); ?>
                                <div id="main-menu">
                                <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('class' => array('main-menu', 'menu'), ), 'heading' => array('text' => t('Main menu'), 'level' => 'h2', 'class' => array('element-invisible'), ), )); ?>
                                </div>
                            </nav>
                        </div>
                        <!-- EOF: #main-navigation -->
                    </div>
                    <!-- EOF:#header-inside-right -->                        
                </div>
            </div>
        <!-- </div> -->
    </div>
</header>
<!-- EOF: #header -->

<!-- # Breadcrumb -->
<?php if ($breadcrumb && theme_get_setting("breadcrumb_display")) :?>

<div id="page-intro" class="clearfix">
    <div id="page-intro-inside" class="clearfix internal-banner no-internal-banner-image">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="breadcrumb" class="clearfix">
                        <div id="breadcrumb-inside" class="clearfix">
                        <?php print $breadcrumb; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>
<!-- EOF:#Breadcrumb -->

<!-- #page -->
<div id="page" class="clearfix">

    <!-- #messages-console -->
    <?php if ($messages):?>
    <div id="messages-console" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <?php print $messages; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <!-- EOF: #messages-console -->

    <?php if ($page['highlighted']):?>
    <!-- #highlighted -->
    <div id="highlighted">
        <div class="container">

            <!-- #highlighted-inside -->
            <div id="highlighted-inside" class="clearfix">
                <div class="row">
                    <div class="col-md-12">
                    <?php print render($page['highlighted']); ?>
                    </div>
                </div>
            </div>
            <!-- EOF:#highlighted-inside -->

        </div>
    </div>
    <!-- EOF: #highlighted -->
    <?php endif; ?>

    <!-- #main-content -->
    <div id="main-content">
        <div class="container">

            <div class="row">

                <?php if ($page['sidebar_first']):?>
                <aside class="<?php print $sidebar_first_grid_class; ?>">
                    <!--#sidebar-->
                    <section id="sidebar-first" class="sidebar clearfix responsive">
                    <?php print render($page['sidebar_first']); ?>
                    </section>
                    <!--EOF:#sidebar-->
                </aside>
                <?php endif; ?>

                <section class="<?php print $main_grid_class; ?>">

                    <!-- #promoted -->
                    <?php if ($page['promoted']):?>
                        <div id="promoted" class="clearfix">
                            <div id="promoted-inside" class="clearfix">
                            <?php print render($page['promoted']); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!-- EOF: #promoted -->

                    <!-- #main -->
                    <div id="main" class="clearfix">

                        <?php print render($title_prefix); ?>
                        <?php if ($title): ?><h1 class="title" style="font-size: 17px;" id="page-title"><?php print $title; ?></h1><?php endif; ?>
                        <?php print render($title_suffix); ?>

                        <!-- #tabs -->
                        <?php if ($tabs):?>
                            <div class="tabs">
                            <?php print render($tabs); ?>
                            </div>
                        <?php endif; ?>
                        <!-- EOF: #tabs -->

                        <?php print render($page['help']); ?>

                        <!-- #action links -->
                        <?php if ($action_links):?>
                            <ul class="action-links">
                            <?php print render($action_links); ?>
                            </ul>
                        <?php endif; ?>
                        <!-- EOF: #action links -->

                        <?php if (theme_get_setting('frontpage_content_print') || !drupal_is_front_page()):?> 
                        <?php print render($page['content']); ?>
                        <!-- <?php //print $feed_icons; ?> -->
                        <?php endif; ?>

                    </div>
                    <!-- EOF:#main -->

                </section>

                <?php if ($page['sidebar_second']):?>
                <aside class="<?php print $sidebar_second_grid_class; ?>">
                    <!--#sidebar-->
                    <section id="sidebar-second" class="sidebar clearfix">
                    <?php print render($page['sidebar_second']); ?>
                    </section>
                    <!--EOF:#sidebar-->
                </aside>
                <?php endif; ?>
                
            </div>

        </div>
    </div>
    <!-- EOF:#main-content -->

</div>
<!-- EOF: #page -->

<!-- <?php //if ($page['bottom_content']):?> -->
<!-- #bottom-content -->
<div id="bottom-content" class="clearfix">
    <div class="container">

        <!-- #bottom-content-inside -->
        <div id="bottom-content-inside" class="clearfix">
            <div class="bottom-content-area">
                <div class="row">
                    <div class="col-md-12">
                    <!-- <?php //print render($page['bottom_content']); ?> -->
                    <div class="row text-center">
                    	<div class="col-md-4">
                    		© ФГАОУ ВО «УРФУ ИМЕНИ ПЕРВОГО ПРЕЗИДЕНТА РОССИИ Б.Н. ЕЛЬЦИНА»
                    	</div>
                    	<div class="col-md-4 footer-text">
                    		ИНСТИТУТ ФУНДАМЕНТАЛЬНОГО ОБРАЗОВАНИЯ
                    		<p>INFO.URFU.RU</p>
                    		<p><strong>ДИРЕКЦИЯ ИНФО:</strong> (343) 375-97-00</p>
                    		<p><strong>ОТБОРОЧНАЯ КОМИССИЯ ИНФО:</strong> (343) 375-93-71</p>
                    		<p><strong>ДЕКАНАТ:</strong> (343) 375-45-34</p>
                    	</div>
                    	<div class="col-md-4 footer-text">
                    		МЫ В СОЦИАЛЬНЫХ СЕТЯХ:
                    		<p>
                    		<a href="http://vk.com/urfu_info"><img class="logo_img" src="/sites/all/themes/scholarly_lite/images/vkLogo.png" alt="VK"></a>
                    		<a href="http://www.facebook.com/info.urfu"><img class="logo_img" src="/sites/all/themes/scholarly_lite/images/fbLogo.png" alt="Facebook"></a>
                    		<a href="https://info.urfu.ru/ru/?type=2"><img class="logo_img" src="/sites/all/themes/scholarly_lite/images/rssLogo.png" alt="RSS"></a>
                    		</p>
                    	</div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- EOF:#bottom-content-inside -->

    </div>
</div>
<!-- EOF: #bottom-content -->
<!-- <?php //endif; ?> -->

<?php if ($page['footer_top_left'] || $page['footer_top_right']) :?>
<!-- #footer-top -->
<div id="footer-top" class="clearfix <?php print $footer_top_regions; ?>">
    <div class="container">

        <!-- #footer-top-inside -->
        <div id="footer-top-inside" class="clearfix">
            <div class="row">
            
            <?php if ($page['footer_top_left']) :?>
            <div class="<?php print $footer_top_left_grid_class; ?>">
                <!-- #footer-top-left -->
                <div id="footer-top-left" class="clearfix">
                    <div class="footer-top-area">
                        <?php print render($page['footer_top_left']); ?>
                    </div>
                </div>
                <!-- EOF:#footer-top-left -->
            </div>
            <?php endif; ?>
            
            <?php if ($page['footer_top_right']) :?>
            <div class="<?php print $footer_top_right_grid_class; ?>">
                <!-- #footer-top-right -->
                <div id="footer-top-right" class="clearfix">
                    <div class="footer-top-area">                    
                        <?php print render($page['footer_top_right']); ?>
                    </div>
                </div>
                <!-- EOF:#footer-top-right -->
            </div>
            <?php endif; ?>
            
            </div>
        </div>
        <!-- EOF: #footer-top-inside -->

    </div>
</div>
<!-- EOF: #footer-top -->    
<?php endif; ?>
