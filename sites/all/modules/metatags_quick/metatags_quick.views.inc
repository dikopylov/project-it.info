<?php
/**
 * @file
 *  Views include file for metatags_quick
 */

/**
 * Implements hook_views_post_render().
 */
function metatags_quick_views_post_render(&$view, $output, $cache) {
  // If the view is a page display, has a path and it is a taxonomy term path.
  if (($view->display_handler instanceof views_plugin_display_page) && $view->display_handler->has_path() && ($view->get_path() == 'taxonomy/term/%')) {

    // Handle single and multiple values in tid argument.
    if (!empty($view->args[0])) {
      if (is_numeric($view->args[0])) {
        // We have only numbers in tid argument.
        $term = taxonomy_term_load($view->args[0]);
      }
      else {
        // Tid looks like "1,2,3" or "1+2+3".
        $views_break = views_break_phrase_string($view->args[0]);
        if (!empty($views_break->value)) {
          // Remove empty (FALSE, NULL, 0) and duplicate tids.
          $tids = array_unique(array_filter($views_break->value));
          foreach ($tids as $tid) {
            $term = taxonomy_term_load($tid);
            if ($term === FALSE) {
              // We not find term by tid, move to next.
              continue;
            }
            else {
              // It's imposible to display fields for each tid.
              // So just dislpay fields for first term.
              break;
            }
          }
        }
      }
    }

    if (empty($term->vocabulary_machine_name)) {
      return;
    }

    $fields = field_info_instances('taxonomy_term', $term->vocabulary_machine_name);

    foreach ($fields as $name => $instance) {
      $settings = field_get_display($instance, 'full', 'taxonomy_term');
      // Check for fields which use this module for display, and render them.
      if ($settings['module'] = 'metatags_quick') {
        field_view_field('taxonomy_term', $term, $name, $settings);
      }
    }
  }
}
